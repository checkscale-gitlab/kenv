mod apps;
mod clusters;
mod tools;

use crate::prelude::*;
use clap::{App, ArgMatches};
pub use tools::update as update_tools;

pub trait Command {
  fn spec<'a>(&self) -> App<'a, 'a>;
  fn run(&self, matches: &ArgMatches) -> Option<AppResult<()>>;
}

pub fn all_commands() -> Vec<Box<dyn Command>> {
  vec![
    Box::new(apps::AppsCmd {}),
    Box::new(clusters::ClustersCmd {}),
    Box::new(tools::ToolsCmd {}),
  ]
}
