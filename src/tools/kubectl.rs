use crate::prelude::*;

pub struct Kubectl {}

#[derive(Debug)]
pub struct Context {
  pub name: String,
  pub cluster: String,
  pub is_current: bool,
}

impl tools::Tool for Kubectl {
  fn can_download(&self) -> bool {
    true
  }

  fn get_name(&self) -> &'static str {
    "kubectl"
  }

  fn get_latest_version_url(&self) -> &'static str {
    "https://storage.googleapis.com/kubernetes-release/release/stable.txt"
  }

  fn get_download_info(&self) -> Option<(&'static str, Option<&'static str>)> {
    match std::env::consts::OS {
      "linux" => Some(("https://storage.googleapis.com/kubernetes-release/release/v{}/bin/linux/amd64/kubectl", None)),
      "windows" => Some(("https://storage.googleapis.com/kubernetes-release/release/v{}/bin/windows/amd64/kubectl.exe", None)),
      _ => None,
    }
  }

  fn get_version(&self) -> AppResult<String> {
    let output = self
      .mk_command()?
      .arg("version")
      .arg("--client")
      .arg("--short")
      .output()?;
    let output = String::from_utf8(output.stdout)?;
    let version = output
      .trim_end()
      .split(' ')
      .last()
      .unwrap_or_default()
      .trim_start_matches('v')
      .to_owned();
    Ok(version)
  }
}

impl Kubectl {
  pub fn use_context<S: AsRef<str>>(context: S) -> AppResult<()> {
    let output = Self::mk_command(&Self {})?
      .arg("config")
      .arg("use-context")
      .arg(context.as_ref())
      .output()?;
    if !output.status.success() {
      return err(ErrorKind::Other, "failed to use context");
    }
    Ok(())
  }

  pub fn list_contexts() -> AppResult<Vec<Context>> {
    let output = Self::mk_command(&Self {})?
      .arg("config")
      .arg("get-contexts")
      .output()?;
    if !output.status.success() {
      return err(ErrorKind::Other, "failed to get contexts");
    }
    let output = String::from_utf8(output.stdout)?;
    let lines = output.split('\n').skip(1).collect::<Vec<_>>();
    let mut result = vec![];
    for line in lines {
      let parts = line
        .split(' ')
        .filter(|x| !x.is_empty())
        .collect::<Vec<_>>();
      let n = parts.len();
      if n < 3 {
        continue;
      }
      let is_current = n == 4;
      let mut index = 0;
      if is_current {
        index = 1;
      }
      let name = parts[index].to_owned();
      let cluster = parts[index + 1].to_owned();
      let context = Context {
        name,
        cluster,
        is_current,
      };
      result.push(context);
    }
    Ok(result)
  }

  pub fn get_current_context() -> AppResult<Option<Context>> {
    let contexts = Self::list_contexts()?;
    for ctx in contexts {
      if ctx.is_current {
        return Ok(Some(ctx));
      }
    }
    Ok(None)
  }

  pub fn list_namespaces<S: AsRef<str>>(context: S) -> AppResult<Vec<String>> {
    let output = Self::mk_command(&Self {})?
      .arg("--context")
      .arg(context.as_ref())
      .arg("get")
      .arg("namespaces")
      .output()?;
    if !output.status.success() {
      return err(ErrorKind::Other, "failed to list namespaces");
    }
    let mut result = vec![];
    let output = String::from_utf8(output.stdout)?;
    let lines = output.split('\n').skip(1);
    for line in lines {
      let parts = line
        .split(' ')
        .filter(|x| !x.is_empty())
        .collect::<Vec<_>>();
      if parts.len() != 3 {
        continue;
      }
      result.push(parts[0].to_owned());
    }
    Ok(result)
  }

  pub fn list_pods<S: AsRef<str>>(
    context: S,
    namespace: S,
  ) -> AppResult<Vec<String>> {
    let output = Self::mk_command(&Self {})?
      .arg("--context")
      .arg(context.as_ref())
      .arg("-n")
      .arg(namespace.as_ref())
      .arg("get")
      .arg("pods")
      .output()?;
    if !output.status.success() {
      return err(ErrorKind::Other, "failed to list pods");
    }
    let mut result = vec![];
    let output = String::from_utf8(output.stdout)?;
    let lines = output.split('\n').skip(1);
    for line in lines {
      let parts = line
        .split(' ')
        .filter(|x| !x.is_empty())
        .collect::<Vec<_>>();
      if parts.len() != 5 {
        continue;
      }
      result.push(parts[0].to_owned());
    }
    Ok(result)
  }

  pub fn list_crds<S: AsRef<str>>(context: S) -> AppResult<Vec<String>> {
    let output = Self::mk_command(&Self {})?
      .arg("--context")
      .arg(context.as_ref())
      .arg("get")
      .arg("crd")
      .output()?;
    if !output.status.success() {
      return err(ErrorKind::Other, "failed to list CRDs");
    }
    let mut result = vec![];
    let output = String::from_utf8(output.stdout)?;
    let lines = output.split('\n').skip(1);
    for line in lines {
      let parts = line
        .split(' ')
        .filter(|x| !x.is_empty())
        .collect::<Vec<_>>();
      if parts.len() != 2 {
        continue;
      }
      result.push(parts[0].to_owned());
    }
    Ok(result)
  }

  pub fn wait_for_ready_pods<S: AsRef<str>>(
    context: S,
    timeout: S,
    namespace: Option<S>,
  ) -> AppResult<bool> {
    let mut command = Self::mk_command(&Self {})?;
    let mut command = command
      .arg("--context")
      .arg(context.as_ref())
      .arg("wait")
      .arg("--timeout")
      .arg(timeout.as_ref());
    if let Some(namespace) = namespace {
      command = command.arg("-n").arg(namespace.as_ref());
    } else {
      command = command.arg("-A");
    }
    command = command
      .arg("--for")
      .arg("condition=Ready")
      .arg("pods")
      .arg("--all");
    let output = command.output()?;
    Ok(output.status.success())
  }

  pub fn wait_for_established_crd<S: AsRef<str>>(
    context: S,
    timeout: S,
    crd: S,
  ) -> AppResult<bool> {
    let crd = format!("crd/{}", crd.as_ref());
    let mut command = Self::mk_command(&Self {})?;
    let command = command
      .arg("--context")
      .arg(context.as_ref())
      .arg("wait")
      .arg("--timeout")
      .arg(timeout.as_ref())
      .arg("--for")
      .arg("condition=established")
      .arg(crd.as_str());
    let output = command.output()?;
    Ok(output.status.success())
  }

  pub fn wait_for_established_crds<S: AsRef<str>>(
    context: S,
    timeout: S,
  ) -> AppResult<()> {
    let crds = Self::list_crds(context.as_ref())?;
    for crd in crds {
      Self::wait_for_established_crd(
        context.as_ref(),
        timeout.as_ref(),
        crd.as_str(),
      )?;
    }
    Ok(())
  }
}
