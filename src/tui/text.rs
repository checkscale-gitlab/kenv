pub enum StateText<S: AsRef<str>> {
  Ok(S),
  Warn(S),
  Err(S),
  Log(S),
}

impl<S: AsRef<str>> StateText<S> {
  pub fn ok(msg: S) -> Option<StateText<S>> {
    Some(StateText::Ok(msg))
  }

  pub fn warn(msg: S) -> Option<StateText<S>> {
    Some(StateText::Warn(msg))
  }

  pub fn err(msg: S) -> Option<StateText<S>> {
    Some(StateText::Err(msg))
  }

  pub fn log(msg: S) -> Option<StateText<S>> {
    Some(StateText::Log(msg))
  }
}

pub struct Text<S: AsRef<str>> {
  pub prefix: S,
  pub msg: StateText<S>,
  pub suffix: S,
}

impl<S: AsRef<str>> Text<S> {
  pub fn ok(prefix: S, msg: S, suffix: S) -> Self {
    Self {
      prefix,
      msg: StateText::Ok(msg),
      suffix,
    }
  }

  pub fn warn(prefix: S, msg: S, suffix: S) -> Self {
    Self {
      prefix,
      msg: StateText::Warn(msg),
      suffix,
    }
  }

  pub fn err(prefix: S, msg: S, suffix: S) -> Self {
    Self {
      prefix,
      msg: StateText::Err(msg),
      suffix,
    }
  }

  pub fn log(prefix: S, msg: S, suffix: S) -> Self {
    Self {
      prefix,
      msg: StateText::Log(msg),
      suffix,
    }
  }
}
