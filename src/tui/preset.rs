use ansi_term::Color;
use std::sync::{Arc, Mutex};

#[derive(Copy, Clone)]
pub struct Preset {
  pub border_color: Color,
  pub highlight_color: Color,
  pub header_color: Color,
  pub success_color: Color,
  pub warning_color: Color,
  pub failure_color: Color,
  pub normal_color: Color,
}

const BRIGHT_PRESET: Preset = Preset {
  border_color: Color::RGB(127, 127, 127),
  highlight_color: Color::RGB(255, 126, 0),
  header_color: Color::RGB(227, 38, 54),
  success_color: Color::RGB(0, 153, 51),
  warning_color: Color::RGB(255, 126, 0),
  failure_color: Color::RGB(255, 85, 85),
  normal_color: Color::RGB(93, 138, 168),
};

lazy_static! {
  pub static ref CURRENT_PRESET: Arc<Mutex<Preset>> =
    Arc::new(Mutex::new(BRIGHT_PRESET));
}

pub fn paint<S: AsRef<str>>(color: &Color, input: S) -> String {
  if std::env::consts::OS == "windows" {
    input.as_ref().to_owned()
  } else {
    format!("{}", color.paint(input.as_ref()))
  }
}
