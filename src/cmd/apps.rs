mod list;
mod rollout;

use std::path::PathBuf;

use crate::prelude::*;
use clap::{Arg, ArgMatches, SubCommand};
use list::*;
use rollout::*;

enum AppsCmdArgs {
  List(AppsListArgs),
  Rollout(AppsRolloutArgs),
}

pub struct AppsCmd {}

const APPS: &str = "apps";
const APPS_LIST: &str = "list";
const APPS_LIST_CLUSTER_NAME: &str = "cluster-name";
const APPS_LIST_NAMESPACE: &str = "namespace";
const APPS_ROLLOUT: &str = "rollout";
const APPS_ROLLOUT_CLUSTER_NAME: &str = "cluster-name";
const APPS_ROLLOUT_PATH: &str = "path";
const APPS_ROLLOUT_SYSTEM_NAMESPACE: &str = "system-namespace";
const APPS_ROLLOUT_EXTRA_NAMESPACE: &str = "extra-namespace";
const APPS_ROLLOUT_ALL_NAMESPACES: &str = "all-namespaces";
const APPS_ROLLOUT_SKIP_IF_ALREADY_INSTALLED: &str =
  "skip-if-already-installed";
const APPS_ROLLOUT_TIMEOUT: &str = "timeout";

impl cmd::Command for AppsCmd {
  fn spec<'a>(&self) -> clap::App<'a, 'a> {
    SubCommand::with_name(APPS)
      .about("Manage applications with Helm/Kustomize")
      .aliases(&["a", "app"])
      .subcommand(
        SubCommand::with_name(APPS_LIST)
          .about("List managed Helm applications")
          .alias("ls")
          .settings(CMD_SETTINGS.as_slice())
          .arg(
            Arg::with_name(APPS_LIST_CLUSTER_NAME)
              .required(false)
              .takes_value(true)
              .long(APPS_LIST_CLUSTER_NAME)
              .short("n")
              .default_value("local")
              .help("The name of the cluster"),
          )
          .arg(
            Arg::with_name(APPS_LIST_NAMESPACE)
              .required(false)
              .takes_value(true)
              .long(APPS_LIST_NAMESPACE)
              .help("The name of the namespace (otherwise, all)"),
          ),
      )
      .subcommand(
        SubCommand::with_name(APPS_ROLLOUT)
          .about("Rollout applications")
          .aliases(&["r", "roll", "i", "ins", "inst", "install"])
          .settings(CMD_SETTINGS.as_slice())
          .arg(
            Arg::with_name(APPS_ROLLOUT_CLUSTER_NAME)
              .required(false)
              .takes_value(true)
              .long(APPS_ROLLOUT_CLUSTER_NAME)
              .short("n")
              .default_value("local")
              .help("The name of the cluster"),
          )
          .arg(
            Arg::with_name(APPS_ROLLOUT_PATH)
              .required(false)
              .takes_value(true)
              .long(APPS_ROLLOUT_PATH)
              .short("p")
              .default_value("./")
              .help("The path to application's repository"),
          )
          .arg(
            Arg::with_name(APPS_ROLLOUT_SYSTEM_NAMESPACE)
              .required(false)
              .takes_value(true)
              .long(APPS_ROLLOUT_SYSTEM_NAMESPACE)
              .default_value("system")
              .help("The system namespace (rolled out first)"),
          )
          .arg(
            Arg::with_name(APPS_ROLLOUT_EXTRA_NAMESPACE)
              .required(false)
              .takes_value(true)
              .multiple(true)
              .long(APPS_ROLLOUT_EXTRA_NAMESPACE)
              .short("x")
              .help("The extra pre-configured namespaces to install"),
          )
          .arg(
            Arg::with_name(APPS_ROLLOUT_ALL_NAMESPACES)
              .required(false)
              .takes_value(false)
              .long(APPS_ROLLOUT_ALL_NAMESPACES)
              .short("a")
              .conflicts_with(APPS_ROLLOUT_EXTRA_NAMESPACE)
              .help("Install all pre-configured namespaces"),
          )
          .arg(
            Arg::with_name(APPS_ROLLOUT_SKIP_IF_ALREADY_INSTALLED)
              .required(false)
              .takes_value(false)
              .long(APPS_ROLLOUT_SKIP_IF_ALREADY_INSTALLED)
              .short("s")
              .help("Skips upgrading a chart if it was already installed"),
          )
          .arg(
            Arg::with_name(APPS_ROLLOUT_TIMEOUT)
              .required(false)
              .takes_value(true)
              .long(APPS_ROLLOUT_TIMEOUT)
              .short("t")
              .default_value("5m0s")
              .help("Time to wait for any application to install"),
          ),
      )
  }

  fn run(&self, matches: &clap::ArgMatches) -> Option<AppResult<()>> {
    if let Some(args) = self.match_args(matches) {
      return match args {
        AppsCmdArgs::List(args) => Some(list(&args)),
        AppsCmdArgs::Rollout(args) => Some(rollout(&args)),
      };
    }
    None
  }
}

impl AppsCmd {
  fn match_args(&self, matches: &ArgMatches) -> Option<AppsCmdArgs> {
    matches
      .subcommand_matches(APPS)
      .map(|matches| {
        if let Some(m) = matches.subcommand_matches(APPS_LIST) {
          let cluster_name =
            m.value_of(APPS_LIST_CLUSTER_NAME).unwrap().to_owned();
          let namespace = m.value_of(APPS_LIST_NAMESPACE).map(|x| x.to_owned());
          return Some(AppsCmdArgs::List(AppsListArgs {
            cluster_name,
            namespace,
          }));
        }
        if let Some(m) = matches.subcommand_matches(APPS_ROLLOUT) {
          let cluster_name =
            m.value_of(APPS_ROLLOUT_CLUSTER_NAME).unwrap().to_owned();
          let path = m.value_of(APPS_ROLLOUT_PATH).unwrap().to_owned();
          let path = PathBuf::from(path);
          let system_namespace = m
            .value_of(APPS_ROLLOUT_SYSTEM_NAMESPACE)
            .map(|x| x.to_owned());
          let mut extra_namespaces = m
            .values_of(APPS_ROLLOUT_EXTRA_NAMESPACE)
            .unwrap_or_default()
            .map(|x| x.to_owned())
            .collect::<Vec<String>>();
          extra_namespaces.sort();
          let all_namespaces = m.is_present(APPS_ROLLOUT_ALL_NAMESPACES);
          let skip_if_already_installed =
            m.is_present(APPS_ROLLOUT_SKIP_IF_ALREADY_INSTALLED);
          let timeout = m.value_of(APPS_ROLLOUT_TIMEOUT).unwrap().to_owned();
          return Some(AppsCmdArgs::Rollout(AppsRolloutArgs {
            cluster_name,
            path,
            system_namespace,
            extra_namespaces,
            all_namespaces,
            skip_if_already_installed,
            timeout,
          }));
        }
        None
      })
      .flatten()
  }
}
