use crate::prelude::*;
use crate::tools::helm::Helm;
use crate::tools::Kubectl;
use std::{path::PathBuf, thread, time};
use tools::kind::Kind;

#[derive(Debug)]
pub struct AppsRolloutArgs {
  pub cluster_name: String,
  pub path: PathBuf,
  pub system_namespace: Option<String>,
  pub extra_namespaces: Vec<String>,
  pub all_namespaces: bool,
  pub skip_if_already_installed: bool,
  pub timeout: String,
}

pub fn rollout(args: &AppsRolloutArgs) -> AppResult<()> {
  Tui::chapter(format!(
    "Rolling out applications to [{}] cluster",
    &args.cluster_name
  ));
  let mut invalid = false;
  if let Err(error) = validate_cluster(args) {
    Tui::redln(format!("{}", error));
    invalid = true;
  }
  if let Err(error) = validate_repository(args) {
    Tui::redln("failed");
    let lines = format!("{}", error)
      .split('\n')
      .map(|x| x.to_owned())
      .collect::<Vec<String>>();
    for line in lines {
      Tui::writeln(Text::warn(" |", line.as_str(), ""), None);
    }
    invalid = true;
  }
  if invalid {
    Tui::divider();
    return Ok(());
  }
  if let Some(system_namespace) = &args.system_namespace {
    rollout_releases(system_namespace, args, true)?;
  }
  let namespaces = if args.all_namespaces {
    list_all_non_system_namespaces(args)?
  } else {
    args.extra_namespaces.iter().map(|x| x.to_owned()).collect()
  };
  for namespace in namespaces {
    rollout_releases(&namespace, args, args.skip_if_already_installed)?;
  }
  Tui::divider();
  Ok(())
}

fn validate_cluster(args: &AppsRolloutArgs) -> AppResult<()> {
  Tui::write(Text::ok(">>", "Validating cluster", "|"), None);
  if !Kind::list_clusters()?.contains(&args.cluster_name) {
    return err(ErrorKind::NotFound, "cannot find cluster");
  }
  Tui::greenln("valid");
  Ok(())
}

fn validate_repository(args: &AppsRolloutArgs) -> AppResult<()> {
  Tui::write(Text::ok(">>", "Validating repository", "|"), None);
  let mut errors = vec![];
  let root_path = args.path.to_owned();
  if !root_path.exists() {
    errors.push(format!(
      "  * [{}] path is missing",
      root_path.to_string_lossy()
    ));
  }
  let charts_path = root_path.join("charts");
  if !charts_path.exists() {
    errors.push(format!(
      "  * [{}] path is missing",
      charts_path.to_string_lossy()
    ));
  }
  if let Some(system_namespace) = &args.system_namespace {
    let system_namespace_path = charts_path.join(system_namespace);
    if !system_namespace_path.exists() {
      errors.push(format!(
        "  * [{}] path is missing",
        system_namespace_path.to_string_lossy()
      ));
    }
  }
  let kustomize_path = root_path.join("kustomize");
  if !kustomize_path.exists() {
    errors.push(format!(
      "  * [{}] path is missing",
      kustomize_path.to_string_lossy()
    ));
  }
  if !errors.is_empty() {
    errors.insert(
      0,
      "ERROR: repository folder structure is invalid".to_owned(),
    );
    let err_msg = errors.join("\n");
    return err(ErrorKind::InvalidData, err_msg.as_str());
  }
  Tui::greenln("valid");
  Ok(())
}

fn list_all_non_system_namespaces(
  args: &AppsRolloutArgs,
) -> AppResult<Vec<String>> {
  let charts_path = args.path.join("charts");
  let system_namespace = if let Some(ns) = &args.system_namespace {
    ns.to_owned()
  } else {
    String::new()
  };
  let mut result = vec![];
  for item in charts_path.read_dir()? {
    let path = item?.path();
    if !path.is_dir() {
      continue;
    }
    let name = path.file_name().unwrap().to_str().unwrap().to_owned();
    if name != system_namespace {
      result.push(name);
    }
  }
  result.sort();
  Ok(result)
}

fn rollout_releases<S: AsRef<str>>(
  namespace: S,
  args: &AppsRolloutArgs,
  skip_if_already_installed: bool,
) -> AppResult<()> {
  let msg = format!("Rolling out [{}] releases", namespace.as_ref());
  Tui::write(Text::ok(">>", msg.as_str(), "|"), None);
  let mut releases = vec![];
  let path = args.path.join("charts").join(namespace.as_ref());
  if path.exists() {
    let items = std::fs::read_dir(path)?;
    for item in items {
      let path = item?.path();
      if path.is_dir() {
        let name = path.file_name().unwrap().to_str().unwrap().to_owned();
        if !name.starts_with('_') {
          releases.push(name);
        }
      }
    }
  }
  releases.sort();
  if releases.is_empty() {
    Tui::yellowln("skipped");
  } else {
    Tui::greenln("");
  }
  let mut index = 0;
  for release in releases {
    index += 1;
    let prefix = format!("{msg:>width$}", msg = index.to_string(), width = 2);
    Tui::write(Text::ok(prefix.as_str(), release.as_str(), "|"), None);
    let chart_path = args
      .path
      .join("charts")
      .join(namespace.as_ref())
      .join(&release);
    let already_installed =
      Helm::list_releases(args.cluster_name.as_str(), namespace.as_ref())?
        .iter()
        .any(|x| x.name == release);
    if skip_if_already_installed && already_installed {
      Tui::yellowln("exists");
    } else {
      Tui::yellowln("...");
      Helm::update_dependencies(
        args.cluster_name.as_str(),
        chart_path.to_owned(),
        false,
      )?;
      Helm::upgrade_install_wait(
        args.cluster_name.as_str(),
        Some(namespace.as_ref()),
        vec![],
        &release,
        &chart_path.to_str().unwrap().to_owned(),
        args.timeout.as_str(),
        true,
      )?;
      let context = Kind::cluster_name_to_context(args.cluster_name.as_str());
      Kubectl::wait_for_established_crds(context.as_str(), "1m")?;
      if release.contains("cert-manager") || release.contains("certmanager") {
        thread::sleep(time::Duration::from_secs(20));
      }
    }
  }
  Ok(())
}
