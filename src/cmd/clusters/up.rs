use std::fs::File;
use std::io::Write;
use std::path::PathBuf;
use std::thread;
use std::time::Duration;

use crate::prelude::*;
use crate::tools::docker::Docker;
use crate::tools::helm::Helm;
use crate::tools::kind::Kind;
use crate::tools::tigera_operator::TigeraOperator;
use crate::tools::Kubectl;

#[derive(Debug)]
pub struct ClusterStartArgs {
  pub name: String,
  pub version: String,
  pub workers: u8,
  pub enable_calico_cni: bool,
  pub enable_image_registry: bool,
  pub image_registry_name: String,
  pub image_registry_port: u16,
  pub image_registry_ui_name: String,
  pub image_registry_ui_port: u16,
  pub exposed_ports: Vec<(u16, u16)>,
  pub mounts: Vec<(PathBuf, PathBuf)>,
  pub custom_registry: Option<String>,
}

impl Default for ClusterStartArgs {
  fn default() -> Self {
    ClusterStartArgs {
      name: "local".to_owned(),
      version: "latest".to_owned(),
      workers: 0,
      enable_calico_cni: false,
      enable_image_registry: false,
      image_registry_name: "local-registry".to_owned(),
      image_registry_port: 5000,
      image_registry_ui_name: "local-registry-ui".to_owned(),
      image_registry_ui_port: 5001,
      exposed_ports: vec![],
      mounts: vec![],
      custom_registry: None,
    }
  }
}

impl ClusterStartArgs {
  fn gen_extra_volume_mounts(&self, mkdirs: bool) -> AppResult<String> {
    let mut result = String::new();
    if !self.mounts.is_empty() {
      result = r#"  extraMounts:"#.to_owned();
      for (host_path, container_path) in self.mounts.iter() {
        if mkdirs {
          std::fs::create_dir_all(host_path)?;
        }
        result = format!(
          r#"{}
  - hostPath: {}
    containerPath: {}"#,
          result,
          host_path.to_string_lossy(),
          container_path.to_string_lossy()
        );
      }
    }
    Ok(result)
  }

  #[allow(dead_code)]
  fn to_cluster_config(&self, mkdirs: bool) -> AppResult<String> {
    let mut networking = String::new();
    if self.enable_calico_cni {
      networking = r#"networking:
  disableDefaultCNI: true
  podSubnet: 192.168.0.0/16"#
        .to_owned();
    }
    let mut containerd_patches = String::new();
    if self.enable_image_registry {
      let registry_port = self.image_registry_port.to_string();
      containerd_patches = r#"containerdConfigPatches:
- |-
  [plugins."io.containerd.grpc.v1.cri".registry.mirrors."localhost:{REGISTRY_PORT}"]
    endpoint = ["http://{REGISTRY_NAME}:{REGISTRY_PORT}"]"#
        .replace("{REGISTRY_NAME}", self.image_registry_name.as_str())
        .replace("{REGISTRY_PORT}", registry_port.as_str());
    }
    let mut extra_port_mappings = String::new();
    if !self.exposed_ports.is_empty() {
      extra_port_mappings = r#"  extraPortMappings:"#.to_owned();
      for (host_port, container_port) in self.exposed_ports.iter() {
        extra_port_mappings = format!(
          r#"{}
  - containerPort: {}
    hostPort: {}
    protocol: TCP"#,
          extra_port_mappings, container_port, host_port
        );
      }
    }
    let mut workers = String::new();
    if self.workers > 0 {
      for _ in 0..self.workers {
        workers = format!(
          r#"{}
- role: worker
{mounts}"#,
          workers,
          mounts = self.gen_extra_volume_mounts(mkdirs)?,
        );
      }
    }
    let result = format!(
      r#"kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
name: {cluster_name}
{networking}
{containerd_config_patches}
nodes:
- role: control-plane
  kubeadmConfigPatches:
  - |
    kind: InitConfiguration
    nodeRegistration:
      kubeletExtraArgs:
        node-labels: "ingress-ready=true"
{extra_port_mappings}
{extra_volume_mounts}
{workers}"#,
      cluster_name = &self.name,
      networking = &networking,
      containerd_config_patches = &containerd_patches,
      extra_port_mappings = &extra_port_mappings,
      extra_volume_mounts = self.gen_extra_volume_mounts(mkdirs)?,
      workers = &workers,
    )
    .replace("\n\n", "\n")
    .replace("\n\n", "\n")
    .replace("\n\n", "\n")
    .trim()
    .to_owned();
    Ok(result)
  }
}

pub fn up(args: &ClusterStartArgs) -> AppResult<()> {
  Tui::chapter(format!("Starting up [{}] cluster", args.name));
  if let Err(error) = create_registry(args) {
    Tui::redln(format!("{}", &error));
    Tui::divider();
    return Ok(());
  }
  if let Err(error) = connect_registry_with_kind_network(args) {
    Tui::redln(format!("{}", &error));
    Tui::divider();
    return Ok(());
  }
  if let Err(error) = create_registry_ui(args) {
    Tui::redln(format!("{}", &error));
    Tui::divider();
    return Ok(());
  }
  if let Err(error) = create_cluster(args) {
    Tui::redln(format!("{}", &error));
    Tui::divider();
    return Ok(());
  }
  Tui::divider();
  Ok(())
}

fn create_registry(args: &ClusterStartArgs) -> AppResult<()> {
  Tui::write(Text::ok(">>", "Starting up registry", "|"), None);
  if !args.enable_image_registry {
    Tui::yellowln("skip");
    return Ok(());
  }
  if Docker::is_container_running(&args.image_registry_name)? {
    Tui::yellowln("already exists");
  } else {
    let registry_port = &args.image_registry_port.to_string();
    let config = r#"version: 0.1
log:
  fields:
    service: registry
storage:
  delete:
    enabled: true
  cache:
    blobdescriptor: inmemory
  filesystem:
    rootdirectory: /var/lib/registry
http:
  addr: :5000
  headers:
    X-Content-Type-Options: [nosniff]
    Access-Control-Allow-Origin: ['*']
    Access-Control-Allow-Methods: ['HEAD', 'GET', 'OPTIONS', 'DELETE']
    Access-Control-Allow-Headers: ['Authorization', 'Accept']
    Access-Control-Max-Age: [1728000]
    Access-Control-Allow-Credentials: [true]
    Access-Control-Expose-Headers: ['Docker-Content-Digest']
health:
  storagedriver:
    enabled: true
    interval: 10s
    threshold: 3"#
      .replace("5000", registry_port.as_str());
    let config_path = paths::gen_temp_filepath("yaml");
    let mut file = File::create(&config_path)?;
    file.write_all(config.as_bytes())?;
    let port_mapping = format!(
      "127.0.0.1:{}:{}",
      registry_port.as_str(),
      registry_port.as_str()
    );
    let volume = format!(
      "{}:/custom",
      config_path.parent().unwrap().to_string_lossy()
    );
    let image = match &args.custom_registry {
      Some(custom_registry) => format!("{}registry:2", custom_registry),
      None => "registry:2".to_owned(),
    };
    let config_filename = config_path.file_name().unwrap().to_string_lossy();
    let container_config_path = format!("/custom/{}", config_filename);
    Docker::run_detached(vec![
      "--restart",
      "always",
      "-p",
      port_mapping.as_str(),
      "--name",
      args.image_registry_name.as_str(),
      "-v",
      volume.as_str(),
      image.as_str(),
      "registry",
      "serve",
      container_config_path.as_str(),
    ])?;
    if Docker::is_container_running(&args.image_registry_name)? {
      Tui::greenln("done");
    } else {
      Tui::redln("failed");
    }
  }
  Ok(())
}

fn connect_registry_with_kind_network(
  args: &ClusterStartArgs,
) -> AppResult<()> {
  Tui::write(
    Text::ok(">>", "Connect registry to kind network", "|"),
    None,
  );
  if !args.enable_image_registry {
    Tui::yellowln("skip");
    return Ok(());
  }
  if Docker::try_connect_network_with_container(
    "kind",
    args.image_registry_name.as_str(),
  )? {
    Tui::greenln("done");
  } else {
    Tui::yellowln("already connected");
  }
  Ok(())
}

fn create_registry_ui(args: &ClusterStartArgs) -> AppResult<()> {
  Tui::write(Text::ok(">>", "Starting up registry UI", "|"), None);
  if !args.enable_image_registry {
    Tui::yellowln("skip");
    return Ok(());
  }
  if Docker::is_container_running(&args.image_registry_ui_name)? {
    Tui::yellowln("already exists");
  } else {
    let registry_ui_port = &args.image_registry_ui_port.to_string();
    let port_mapping = format!("127.0.0.1:{}:80", registry_ui_port.as_str());
    let image = match &args.custom_registry {
      Some(custom_registry) => {
        format!("{}joxit/docker-registry-ui:latest", custom_registry)
      }
      None => "joxit/docker-registry-ui:latest".to_owned(),
    };
    let title_env = format!("REGISTRY_TITLE={} image registry", &args.name);
    let url_env = format!(
      "REGISTRY_URL=http://localhost:{}",
      &args.image_registry_port
    );
    Docker::run_detached(vec![
      "--restart",
      "always",
      "-p",
      port_mapping.as_str(),
      "--name",
      args.image_registry_ui_name.as_str(),
      "-e",
      title_env.as_str(),
      "-e",
      url_env.as_str(),
      "-e",
      "SINGLE_REGISTRY=true",
      "-e",
      "DELETE_IMAGES=true",
      image.as_str(),
    ])?;
    if Docker::is_container_running(&args.image_registry_ui_name)? {
      Tui::greenln("done");
    } else {
      Tui::redln("failed");
    }
  }
  Ok(())
}

fn create_cluster(args: &ClusterStartArgs) -> AppResult<()> {
  Tui::write(Text::ok(">>", "Starting up cluster", "|"), None);
  if Kind::list_clusters()?.contains(&args.name) {
    Tui::yellowln("already exists");
  } else {
    Tui::grayln("");
    let spec = args.to_cluster_config(true)?;
    let custom_registry = match &args.custom_registry {
      Some(registry) => registry.to_owned(),
      None => String::new(),
    };
    Kind::start(
      args.name.as_str(),
      args.version.as_str(),
      custom_registry.as_ref(),
      &spec,
      true,
    )?;
    let context = Kind::cluster_name_to_context(args.name.as_str());
    if args.enable_calico_cni {
      let params = match &args.custom_registry {
        Some(custom_registry) => vec![
          format!("installation.registry={}", custom_registry),
          format!("tigeraOperator.registry={}", custom_registry),
          format!("calicoctl.image={}calico/ctl", custom_registry),
        ],
        None => vec![],
      };
      Tui::write(Text::ok(">>", "Finding latest tigera-operator", "|"), None);
      let tigera_operator = TigeraOperator {};
      let tigera_operator_version = tigera_operator.get_latest_version()?;
      let (url_format, _) = tigera_operator.get_download_info().unwrap();
      let chart_format =
        url_format.replace("{}", tigera_operator_version.as_str());
      let mut latest_index = 1;
      for x in 1..10 {
        let index = format!("{}", x);
        let url = chart_format.replace("[]", index.as_str());
        let response = curl(url.as_str(), None)?;
        if response.status().is_redirection() {
          latest_index = x;
        }
      }
      let index = format!("{}", latest_index);
      let tigera_operator_chart = chart_format.replace("[]", index.as_str());
      Tui::greenln(format!(
        "v{}-{}",
        tigera_operator_version.as_str(),
        latest_index
      ));
      Tui::writeln(Text::ok(">>", "Installing calico CNI", ""), None);
      Helm::upgrade_install_wait(
        args.name.as_str(),
        None,
        params,
        "calico",
        tigera_operator_chart.as_str(),
        "5m0s",
        true,
      )?;
      Tui::write(Text::ok(">>", "Starting tigera operator", "|"), None);
      if let Err(error) = Kubectl::wait_for_ready_pods(
        context.as_str(),
        "10m",
        Some("tigera-operator"),
      ) {
        Tui::redln(format!("{}", error));
      } else {
        Tui::greenln("done");
      }
    }
    Tui::write(
      Text::ok(">>", "Waiting for coredns to be scheduled", "|"),
      None,
    );
    for _ in 0..30 {
      let pods = Kubectl::list_pods(context.as_str(), "kube-system")?;
      if pods.iter().any(|x| x.contains("coredns")) {
        Tui::greenln("scheduled");
        break;
      }
      thread::sleep(Duration::from_secs(5));
    }
    for i in 1..4 {
      let msg = format!("Starting all pods [{}]", i);
      Tui::write(Text::ok(">>", msg.as_str(), "|"), None);
      thread::sleep(Duration::from_secs(1));
      let context = Kind::cluster_name_to_context(args.name.as_str());
      if let Err(error) =
        Kubectl::wait_for_ready_pods(context.as_str(), "10m", None)
      {
        Tui::redln(format!("{}", error));
      } else {
        Tui::greenln("done");
      }
    }
  }
  Ok(())
}

#[test]
fn test_cluster_start_args_to_cluster_config_default() {
  let args = ClusterStartArgs::default();
  let config = args.to_cluster_config(false);
  assert!(config.is_ok());
  assert_eq!(
    config.unwrap(),
    r#"kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
name: local
nodes:
- role: control-plane
  kubeadmConfigPatches:
  - |
    kind: InitConfiguration
    nodeRegistration:
      kubeletExtraArgs:
        node-labels: "ingress-ready=true""#
  );
}

#[test]
fn test_cluster_start_args_to_cluster_config_with_multiple_workers() {
  let args = ClusterStartArgs {
    workers: 3,
    ..ClusterStartArgs::default()
  };
  let config = args.to_cluster_config(false);
  assert!(config.is_ok());
  assert_eq!(
    config.unwrap(),
    r#"kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
name: local
nodes:
- role: control-plane
  kubeadmConfigPatches:
  - |
    kind: InitConfiguration
    nodeRegistration:
      kubeletExtraArgs:
        node-labels: "ingress-ready=true"
- role: worker
- role: worker
- role: worker"#
  );
}

#[test]
fn test_cluster_start_args_to_cluster_config_exposing_ports() {
  let args = ClusterStartArgs {
    exposed_ports: vec![(7080, 31080), (7443, 31443)],
    ..ClusterStartArgs::default()
  };
  let config = args.to_cluster_config(false);
  assert!(config.is_ok());
  assert_eq!(
    config.unwrap(),
    r#"kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
name: local
nodes:
- role: control-plane
  kubeadmConfigPatches:
  - |
    kind: InitConfiguration
    nodeRegistration:
      kubeletExtraArgs:
        node-labels: "ingress-ready=true"
  extraPortMappings:
  - containerPort: 31080
    hostPort: 7080
    protocol: TCP
  - containerPort: 31443
    hostPort: 7443
    protocol: TCP"#
  );
}

#[test]
fn test_cluster_start_args_to_cluster_config_with_registry() {
  let args = ClusterStartArgs {
    enable_image_registry: true,
    ..ClusterStartArgs::default()
  };
  let config = args.to_cluster_config(false);
  assert!(config.is_ok());
  assert_eq!(
    config.unwrap(),
    r#"kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
name: local
containerdConfigPatches:
- |-
  [plugins."io.containerd.grpc.v1.cri".registry.mirrors."localhost:5000"]
    endpoint = ["http://local-registry:5000"]
nodes:
- role: control-plane
  kubeadmConfigPatches:
  - |
    kind: InitConfiguration
    nodeRegistration:
      kubeletExtraArgs:
        node-labels: "ingress-ready=true""#
  );
}

#[test]
fn test_cluster_start_args_to_cluster_config_with_registry_on_custom_port() {
  let args = ClusterStartArgs {
    name: "test".to_owned(),
    enable_image_registry: true,
    image_registry_name: "test-registry".to_owned(),
    image_registry_port: 54321,
    ..ClusterStartArgs::default()
  };
  let config = args.to_cluster_config(false);
  assert!(config.is_ok());
  assert_eq!(
    config.unwrap(),
    r#"kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
name: test
containerdConfigPatches:
- |-
  [plugins."io.containerd.grpc.v1.cri".registry.mirrors."localhost:54321"]
    endpoint = ["http://test-registry:54321"]
nodes:
- role: control-plane
  kubeadmConfigPatches:
  - |
    kind: InitConfiguration
    nodeRegistration:
      kubeletExtraArgs:
        node-labels: "ingress-ready=true""#
  );
}

#[test]
fn test_cluster_start_args_to_cluster_config_with_custom_cni() {
  let args = ClusterStartArgs {
    enable_calico_cni: true,
    ..ClusterStartArgs::default()
  };
  let config = args.to_cluster_config(false);
  assert!(config.is_ok());
  assert_eq!(
    config.unwrap(),
    r#"kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
name: local
networking:
  disableDefaultCNI: true
  podSubnet: 192.168.0.0/16
nodes:
- role: control-plane
  kubeadmConfigPatches:
  - |
    kind: InitConfiguration
    nodeRegistration:
      kubeletExtraArgs:
        node-labels: "ingress-ready=true""#
  );
}

#[test]
fn test_cluster_start_args_to_cluster_config_with_extra_mounts() {
  let args = ClusterStartArgs {
    mounts: vec![(PathBuf::from("/tmp"), PathBuf::from("/files"))],
    ..ClusterStartArgs::default()
  };
  let config = args.to_cluster_config(false);
  assert!(config.is_ok());
  assert_eq!(
    config.unwrap(),
    r#"kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
name: local
nodes:
- role: control-plane
  kubeadmConfigPatches:
  - |
    kind: InitConfiguration
    nodeRegistration:
      kubeletExtraArgs:
        node-labels: "ingress-ready=true"
  extraMounts:
  - hostPath: /tmp
    containerPath: /files"#
  );
}

#[test]
fn test_cluster_start_args_to_cluster_config_with_extra_mounts_and_some_workers(
) {
  let args = ClusterStartArgs {
    workers: 3,
    mounts: vec![(PathBuf::from("/tmp"), PathBuf::from("/files"))],
    ..ClusterStartArgs::default()
  };
  let config = args.to_cluster_config(false);
  assert!(config.is_ok());
  assert_eq!(
    config.unwrap(),
    r#"kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
name: local
nodes:
- role: control-plane
  kubeadmConfigPatches:
  - |
    kind: InitConfiguration
    nodeRegistration:
      kubeletExtraArgs:
        node-labels: "ingress-ready=true"
  extraMounts:
  - hostPath: /tmp
    containerPath: /files
- role: worker
  extraMounts:
  - hostPath: /tmp
    containerPath: /files
- role: worker
  extraMounts:
  - hostPath: /tmp
    containerPath: /files
- role: worker
  extraMounts:
  - hostPath: /tmp
    containerPath: /files"#
  );
}
