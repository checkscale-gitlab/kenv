mod list;
mod update;

use crate::prelude::*;
use clap::{App, Arg, ArgMatches, SubCommand};

use list::*;
pub use update::update;

enum ToolsCmdArgs {
  List,
  Update(bool),
}

pub struct ToolsCmd {}

const TOOLS: &str = "tools";
const TOOLS_LIST: &str = "list";
const TOOLS_UPDATE: &str = "update";
const TOOLS_UPDATE_DRY_RUN: &str = "dry-run";

impl cmd::Command for ToolsCmd {
  fn spec<'a>(&self) -> App<'a, 'a> {
    SubCommand::with_name(TOOLS)
      .about("Manage external tools")
      .aliases(&["t", "tool"])
      .subcommand(
        SubCommand::with_name(TOOLS_LIST)
          .about("List tools")
          .alias("ls")
          .settings(CMD_SETTINGS.as_slice()),
      )
      .subcommand(
        SubCommand::with_name(TOOLS_UPDATE)
          .about("Update tools")
          .aliases(&["u", "up", "upd"])
          .settings(CMD_SETTINGS.as_slice())
          .arg(
            Arg::with_name(TOOLS_UPDATE_DRY_RUN)
              .required(false)
              .takes_value(false)
              .long("dry-run")
              .help("Runs update in a dry-run mode"),
          ),
      )
  }

  fn run(&self, matches: &ArgMatches) -> Option<AppResult<()>> {
    if let Some(args) = self.match_args(matches) {
      return match args {
        ToolsCmdArgs::List => Some(list()),
        ToolsCmdArgs::Update(dry_run) => Some(update(dry_run)),
      };
    }
    None
  }
}

impl ToolsCmd {
  fn match_args(&self, matches: &ArgMatches) -> Option<ToolsCmdArgs> {
    matches
      .subcommand_matches(TOOLS)
      .map(|matches| {
        if matches.subcommand_matches(TOOLS_LIST).is_some() {
          return Some(ToolsCmdArgs::List);
        }
        if let Some(matches) = matches.subcommand_matches(TOOLS_UPDATE) {
          let dry_run = matches.is_present(TOOLS_UPDATE_DRY_RUN);
          return Some(ToolsCmdArgs::Update(dry_run));
        }
        None
      })
      .flatten()
  }
}
