use crate::prelude::*;
use std::env::var;

pub fn curl<S: AsRef<str>>(
  url: S,
  redirects: Option<usize>,
) -> AppResult<reqwest::blocking::Response> {
  let mut builder = reqwest::blocking::Client::builder();
  let mut redirect_policy = reqwest::redirect::Policy::none();
  if let Some(redirects) = redirects {
    redirect_policy = reqwest::redirect::Policy::limited(redirects);
  }
  builder = builder.redirect(redirect_policy);
  let http_proxy = var("http_proxy").or_else(|_| var("HTTP_PROXY"));
  let mut proxy = None;
  if url.as_ref().starts_with("http://") {
    if let Ok(http_proxy) = http_proxy {
      let http_proxy = cleanup_proxy_url(&http_proxy);
      proxy = Some(reqwest::Proxy::http(&http_proxy)?);
    }
  }
  let https_proxy = var("https_proxy").or_else(|_| var("HTTPS_PROXY"));
  if url.as_ref().starts_with("https://") {
    if let Ok(https_proxy) = https_proxy {
      let https_proxy = cleanup_proxy_url(&https_proxy);
      proxy = Some(reqwest::Proxy::https(&https_proxy)?);
    }
  }
  if let Some(proxy) = proxy {
    builder = builder.proxy(proxy);
  }
  let client = builder.build()?;
  let response = client.get(url.as_ref()).send();
  if let Err(error) = &response {
    println!("{:#?}", error);
  }
  Ok(response?)
}

fn cleanup_proxy_url<S: AsRef<str>>(url: S) -> String {
  url.as_ref().trim_end_matches('/').to_owned()
}
