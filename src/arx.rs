use crate::prelude::*;
use flate2::read::GzDecoder;
use std::io::{Read, Write};
use std::path::{Path, PathBuf};
use tar::Archive;

pub fn untargz(arx: &Path, target: &Path) -> AppResult<()> {
  let name = target.file_name().unwrap();
  let file = std::fs::File::open(&arx)?;
  let mut archive = Archive::new(GzDecoder::new(file));
  let count = archive
    .entries()?
    .filter_map(|e| e.ok())
    .map(|mut entry| -> std::io::Result<PathBuf> {
      if entry.path()?.ends_with(&name) {
        entry.unpack(&target)?;
        Ok(target.to_owned())
      } else {
        Err(std::io::Error::from(ErrorKind::Other))
      }
    })
    .filter_map(|e| e.ok())
    .count();
  if count == 0 || !target.exists() {
    return err(ErrorKind::Other, "cannot extract file archive");
  }
  Ok(())
}

pub fn unzip(arx: &Path, target: &Path, root: String) -> AppResult<()> {
  let mut name = target.file_name().unwrap().to_str().unwrap().to_owned();
  if !root.is_empty() {
    name = format!("{}/{}", &root, name);
  }
  let file = std::fs::File::open(&arx)?;
  let mut archive = zip::ZipArchive::new(file)?;
  let mut file = archive.by_name(name.as_ref())?;
  let mut content = vec![];
  file.read_to_end(&mut content)?;
  let mut target_file = std::fs::File::create(&target)?;
  target_file.write_all(&content)?;
  Ok(())
}
