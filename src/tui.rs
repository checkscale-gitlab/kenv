mod preset;
mod text;

use preset::*;
use std::{fmt::Display, io::Write};
pub use text::*;

pub struct Tui {}

impl Tui {
  fn preset() -> Preset {
    *CURRENT_PRESET.lock().unwrap()
  }

  pub fn divider() {
    let p = Self::preset();
    println!("{}", paint(&p.border_color, "#-----------------------------------------------------+------------------------------------#"));
  }

  pub fn separator() {
    let p = Self::preset();
    println!("{}", paint(&p.border_color, "############################################################################################"));
  }

  pub fn chapter<S: AsRef<str> + Display>(msg: S) {
    let p = Self::preset();
    println!();
    Self::separator();
    println!(
      "{}{}{}{}{}",
      paint(&p.border_color, "# "),
      paint(&p.highlight_color, ">>> "),
      paint(
        &p.header_color,
        format!("{msg:>width$}", msg = msg, width = 80)
      ),
      paint(&p.highlight_color, " <<<"),
      paint(&p.border_color, " #"),
    );
    Self::separator();
  }

  pub fn chapter_with_subtitle<S: AsRef<str> + Display>(msg: S, subtitle: S) {
    let p = Self::preset();
    println!();
    Self::separator();
    println!(
      "{}{}{}{}{}",
      paint(&p.border_color, "# "),
      paint(&p.highlight_color, ">>> "),
      paint(
        &p.header_color,
        format!("{msg:>width$}", msg = msg, width = 80)
      ),
      paint(&p.highlight_color, " <<<"),
      paint(&p.border_color, " #"),
    );
    println!(
      "{}{}{}{}{}",
      paint(&p.border_color, "# "),
      paint(&p.highlight_color, ">>> "),
      paint(
        &p.normal_color,
        format!("{msg:>width$}", msg = subtitle, width = 80)
      ),
      paint(&p.highlight_color, " <<<"),
      paint(&p.border_color, " #"),
    );
    Self::separator();
  }

  pub fn suffix<S: AsRef<str>>(value: S) {
    let p = Self::preset();
    let value = value.as_ref();
    if !value.is_empty() {
      print!(" {} ", paint(&p.border_color, value));
    }
  }

  pub fn gray<S: AsRef<str>>(value: S) {
    let p = Self::preset();
    let value = value.as_ref();
    if !value.is_empty() {
      print!("{}", paint(&p.border_color, value));
    }
  }

  pub fn grayln<S: AsRef<str>>(value: S) {
    Self::gray(value);
    println!();
  }

  pub fn green<S: AsRef<str>>(value: S) {
    let p = Self::preset();
    let value = value.as_ref();
    if !value.is_empty() {
      print!("{}", paint(&p.success_color, value));
    }
  }

  pub fn greenln<S: AsRef<str>>(value: S) {
    Self::green(value);
    println!();
  }

  pub fn yellow<S: AsRef<str>>(value: S) {
    let p = Self::preset();
    let value = value.as_ref();
    if !value.is_empty() {
      print!("{}", paint(&p.warning_color, value));
    }
  }

  pub fn yellowln<S: AsRef<str>>(value: S) {
    Self::yellow(value);
    println!();
  }

  pub fn red<S: AsRef<str>>(value: S) {
    let p = Self::preset();
    let value = value.as_ref();
    if !value.is_empty() {
      print!("{}", paint(&p.failure_color, value));
    }
  }

  pub fn redln<S: AsRef<str>>(value: S) {
    Self::red(value);
    println!();
  }

  pub fn write<S: AsRef<str>>(text: Text<S>, state: Option<StateText<S>>) {
    let p = Self::preset();
    let pref = text.prefix.as_ref();
    let suf = text.suffix.as_ref();
    let mut msg_n: usize = 50;
    if !pref.is_empty() {
      let pref_n = pref.len();
      print!("{}", paint(&p.border_color, "#"));
      print!(
        "  {} ",
        paint(
          &p.border_color,
          format!("{msg:<width$}", msg = pref, width = pref_n)
        )
      );
      msg_n = msg_n - pref_n - 1;
    }
    let (msg_text, msg_color) = match text.msg {
      StateText::Ok(msg) => (msg, p.normal_color),
      StateText::Warn(msg) => (msg, p.warning_color),
      StateText::Err(msg) => (msg, p.failure_color),
      StateText::Log(msg) => (msg, p.border_color),
    };
    if msg_text.as_ref() != "" {
      print!(
        "{}",
        paint(
          &msg_color,
          format!("{msg:<width$}", msg = msg_text.as_ref(), width = msg_n)
        )
      );
    }
    Self::suffix(suf);
    if let Some(state) = state {
      match state {
        StateText::Ok(text) => Self::green(text),
        StateText::Warn(text) => Self::yellow(text),
        StateText::Err(text) => Self::red(text),
        StateText::Log(text) => Self::gray(text),
      }
    }
    std::io::stdout()
      .flush()
      .expect("Could not flush to stdout");
  }

  pub fn writeln<S: AsRef<str>>(text: Text<S>, state: Option<StateText<S>>) {
    Self::write(text, state);
    println!();
  }

  pub fn fail<S: AsRef<str>>(msg: S) {
    Self::red("ERROR: ");
    Self::yellowln(msg);
  }

  pub fn errorln<S: AsRef<str>, D: std::fmt::Debug>(state: S, error: D) {
    Tui::redln(state);
    let msg = format!("{:#?}", error);
    let lines = msg.split('\n').collect::<Vec<_>>();
    for line in lines {
      Tui::writeln(Text::err("|ERR|", line, ""), None);
    }
  }

  pub fn prompt<S: AsRef<str>>(msg: S) -> String {
    Tui::write(Text::ok("$", msg.as_ref(), ":"), None);
    let mut result = String::new();
    std::io::stdin().read_line(&mut result).unwrap();
    result.trim_end().to_owned()
  }
}
