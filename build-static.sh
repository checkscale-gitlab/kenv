#!/usr/bin/env bash

command -v docker >/dev/null 2>&1 || { echo "$(tput setaf 1)[ error ]$(tput sgr0) docker is not found"; exit 1; }
command -v tar >/dev/null 2>&1 || { echo "$(tput setaf 1)[ error ]$(tput sgr0) tar is not found"; exit 1; }

set -e

ROOT="$(dirname "$(readlink -f "$0")")"
IMAGE="rokulabs/kenv"
TAG=$(grep '^version' < "${ROOT}/Cargo.toml" | awk '{print $NF}' | sed 's/"//g')
TARGET="${ROOT}/kenv"

# build image
docker build -t "${IMAGE}:${TAG}" "${ROOT}"
docker tag "${IMAGE}:${TAG}" "${IMAGE}:latest"

# extract binary
id=$(docker create "${IMAGE}:${TAG}")
docker cp "${id}:/usr/local/bin/kenv" "${TARGET}"
docker rm -f "${id}"

# create archive
pushd "${ROOT}" >/dev/null 2>&1
tar -czf "kenv-v${TAG}.tgz" kenv
popd >/dev/null 2>&1

# cleanup
rm -f "${TARGET}"
